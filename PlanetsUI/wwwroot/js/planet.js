﻿var dt = dt || {};

dt.ss = function () {

    // VARIABLES //////////////////////////////////////////////////////////

    var self = this;

    self.baseUrl = "http://localhost:65529/api/planet";

    // PUBLIC FUNCTIONS ///////////////////////////////////////////////////

    self.callAPI = function (button) {

        var label = $(".api-status-label");
        disableLabel(label);
        setButtonStatus(button, false);

        $.ajax({
            type: 'GET',
            url: self.baseUrl,
            success: function (data) {

                if (data === true) {
                    setupConnectionLabel(label, true);
                }
            },
            error: function (error) {
                console.log("Error on 'callAPI' function: " + error);
                setupConnectionLabel(label, false);
            },
            complete: function () {
                setButtonStatus(button, true);
            }
        });
    };

    self.getPlanets = function (button) {

        setButtonStatus(button, false);

        $.ajax({
            type: 'GET',
            url: "/Home/GetPlanets",
            dataType: "html",
            success: function (view) {

                $("#planet-container").html(view);
            },
            error: function (error) {
                console.log("Error on 'getPlanets' function: " + error);
            },
            complete: function () {
                setButtonStatus(button, true);
                selectDefaultTab();
            }
        });
    };

    self.resetPlanets = function (button) {

        setButtonStatus(button, false);

        $.ajax({
            type: 'GET',
            url: "/Home/ResetPlanets",
            dataType: "html",
            success: function (view) {

                $("#planet-container").html(view);
            },
            error: function (error) {
                console.log("Error on 'resetPlanets' function: " + error);
            },
            complete: function () {
                setButtonStatus(button, true);
                selectDefaultTab();
            }
        });
    };

    self.getPlanetDetail = function (button, id, name) {

        setButtonStatus(button, false);
        var label = $(".api-status-label");

        $(".detail-popup-title").html(name);
        $(".detail-popup-content").html("");

        $.ajax({
            type: 'GET',
            url: "/Home/GetPlanetDetail",
            dataType: "html",
            data: {
                id: id
            },
            success: function (view) {

                $(".detail-popup-content").html(view);
            },
            error: function (error) {
                console.log("Error on 'getPlanetDetail' function: " + error);
            },
            complete: function () {
                setButtonStatus(button, true);
            }
        });
    };

    self.editPlanetDetail = function (button, id, name) {

        setButtonStatus(button, false);
        var label = $(".api-status-label");

        $(".edit-popup-title").html(name);
        $(".edit-popup-content").html("");

        $.ajax({
            type: 'GET',
            url: "/Home/EditPlanetDetail",
            dataType: "html",
            data: {
                id: id
            },
            success: function (view) {

                $(".edit-popup-content").html(view);
            },
            error: function (error) {
                console.log("Error on 'editPlanetDetail' function: " + error);
            },
            complete: function () {
                setButtonStatus(button, true);
            }
        });
    };

    self.selectTab = function (sender) {

        var tab = $(sender).parent();

        // if we are clicking the active tab, return
        if (tab.hasClass("active")) {
            return;
        }

        var index = tab.data("tabId");

        switch (index) {
            case 1:
                $(".tab-grid").addClass("active");
                $(".tab-carousel").removeClass("active");

                $(".tab-2").slideUp(function () {
                    $(".tab-1").slideDown();
                });

                break;
            case 2:
                $(".tab-carousel").addClass("active");
                $(".tab-grid").removeClass("active");
                $(".tab-1").slideUp(function () {
                    $(".tab-2").slideDown();
                });
                break;
        }
    };

    self.saveDetail = function (button) {

        setButtonStatus(button, false);

        var planet = {};

        var formInputs = $(".form-control");

        for (var i = 0; i < formInputs.length; i++) {

            var input = formInputs[i];
            var propertyName = $(input).data("editField");
            var value = $(input).val();

            planet[propertyName] = value;
        }

        $.ajax({
            type: 'POST',
            url: "/Home/SavePlanetDetail",
            dataType: "json",
            data: JSON.stringify(planet),
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                self.getPlanets();

                $(".alert-success").fadeIn(function () {

                    setTimeout(function () {
                        $(".alert-success").fadeOut();
                    }, 1000);

                });


                //$(".alert-success").slideDown("fast", function () {

                //    setTimeout(function () {
                //        $(".alert-success").slideUp("fast");
                //    }, 1000);

                //});

            },
            error: function (error) {
                console.log("Error on 'saveDetail' function: " + error);
            },
            complete: function () {
                setButtonStatus(button, true);
            }
        });
    };

    // PRIVATE FUNCTIONS //////////////////////////////////////////////////

    function getPlanetById(id) {
        var planet = self.planets.filter(s => s.PlanetId == id)[0];

        return planet;
    }

    function addDetailRowToTable(planet) {

        var row = "<tr>";
        row += '<td>' + planet(workItem.type) + '</td>';
        row += '<td>' + workItem.unitCost.toFixed(2) + '</td>';
        row += '<td>' + workItem.unitTime + '</td>';
        row += "</tr>";

        $(".detail-grid tbody").append(row);
    }

    function setButtonStatus(button, enabled) {

        if (button != undefined) {
            if (enabled) {
                $(button).removeClass("disabled");
            } else {
                $(button).addClass("disabled");
            }
        }
    }

    function disableLabel(label) {
        label.removeClass("text-success text-danger").addClass("disabled").text("Connecting to API...");
    }

    function setupConnectionLabel(label, connected) {

        var className = "text-success";
        var text = "API connection OK";

        if (connected) {

            className = "text-success";
            text = "API connection OK";

            label.removeClass("text-danger").addClass("text-success");
            label.html(text);

        } else {

            className = "text-danger";
            text = "API connection failed";

            label.removeClass("text-success").addClass("text-danger");
            label.html(text);

        }

        label.removeClass("disabled");
    }

    function selectDefaultTab() {
        $(".tab-link-1").click();
    }

    return self;
};
