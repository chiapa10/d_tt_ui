﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PlanetsData.Models;
using PlanetsServices;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PlanetsData.Controllers
{
    public class HomeController : Controller
    {
        private readonly PlanetsService _planetService = new PlanetsService();

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> GetPlanets()
        {
            var json = await _planetService.GetPlanets();

            List<Planet> planets = JsonConvert.DeserializeObject<List<Planet>>(json);

            if (planets.Count == 0)
            {
                json = await _planetService.ResetPlanets();
                planets = JsonConvert.DeserializeObject<List<Planet>>(json);
            }

            return PartialView("~/views/Shared/Planets/_Planets.cshtml", planets);
        }

        public async Task<IActionResult> ResetPlanets()
        {
            var json = await _planetService.ResetPlanets();

            List<Planet> planets = JsonConvert.DeserializeObject<List<Planet>>(json);

            return PartialView("~/views/Shared/Planets/_Planets.cshtml", planets);
        }

        public async Task<IActionResult> GetPlanetDetail(int id)
        {
            var json = await _planetService.GetPlanetDetail(id);

            Planet planet = JsonConvert.DeserializeObject<Planet>(json);

            return PartialView("~/views/Shared/Planets/_Detail.cshtml", planet);
        }

        public async Task<IActionResult> EditPlanetDetail(int id)
        {
            var json = await _planetService.GetPlanetDetail(id);

            Planet planet = JsonConvert.DeserializeObject<Planet>(json);

            return PartialView("~/views/Shared/Planets/_EditForm.cshtml", planet);
        }

        [HttpPost]
        public async Task<IActionResult> SavePlanetDetail([FromBody] Planet planet)
        {
            var json = await _planetService.SavePlanetDetail(planet);

            planet = JsonConvert.DeserializeObject<Planet>(json);

            return Json(new { success = true });
        }
    }
}
