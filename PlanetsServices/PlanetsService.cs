﻿using Newtonsoft.Json;
using PlanetsData.Models;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace PlanetsServices
{
    public class PlanetsService
    {
        public async Task<string> GetPlanets()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    using (var response = await client.GetAsync("http://localhost:65529/api/planet/get"))
                    {
                        response.EnsureSuccessStatusCode();

                        var jsonstring = await response.Content.ReadAsStringAsync();

                        return jsonstring;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> ResetPlanets()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    using (var response = await client.GetAsync("http://localhost:65529/api/planet/reset"))
                    {
                        response.EnsureSuccessStatusCode();

                        var jsonstring = await response.Content.ReadAsStringAsync();

                        return jsonstring;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> GetPlanetDetail(int id)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    using (var response = await client.GetAsync($"http://localhost:65529/api/planet/detail?id={id}"))
                    {
                        response.EnsureSuccessStatusCode();

                        var jsonstring = await response.Content.ReadAsStringAsync();

                        return jsonstring;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> SavePlanetDetail(Planet planet)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var json = JsonConvert.SerializeObject(planet);

                    using (var response = await client.PostAsync($"http://localhost:65529/api/planet/save", new StringContent(json, Encoding.UTF8, "application/json")))
                    {
                        response.EnsureSuccessStatusCode();

                        var jsonstring = await response.Content.ReadAsStringAsync();

                        return jsonstring;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
