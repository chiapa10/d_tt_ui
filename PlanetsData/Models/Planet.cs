﻿namespace PlanetsData.Models
{
    public class Planet
    {
        #region Properties

        public int PlanetId { get; set; }
        public string Name { get; set; }
        public decimal Mass { get; set; } // unit: x 10^24kg
        public decimal Diameter { get; set; } // unit: miles
        public decimal DistanceFromSun { get; set; } // unit: thousands of miles
        public decimal Gravity { get; set; } // unit: m/s^2
        public decimal LengthOfDay { get; set; } // unit: hours
        public int NumberOfMoons { get; set; }

        #endregion
    }
}
